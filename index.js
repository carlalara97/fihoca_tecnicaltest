'use strict'

/*
    Per habilitar que els los en la consola tinguin un prefix de temps
*/
require('console-stamp')(console, 'HH:MM:ss.l');

var initRoutines = require('./utils/init_routines');
initRoutines();

var app = require('./app');
//var db = require('./db');
app.appPort = process.env.PORT || 3977;

module.exports = app;

app.listen(app.appPort, function(){
  console.log("Running server... http://localhost:"+app.appPort);
});

//db.connect((err) => {
  //console.log(err);
  //if (err) throw err;
  //console.log("Mysql db connected.");
//});
