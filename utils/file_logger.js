var consts = require('./general_consts');
console.log('CONST: ' + consts.logsDir + " JSON: " + JSON.stringify(consts));
// https://www.npmjs.com/package/simple-node-logger
var opts = {
    errorEventName:'error',
    logDirectory: consts.logsDir, // NOTE: folder must exist and be writable...
    fileNamePattern:'roll-<DATE>.log',
    dateFormat:'YYYY.MM.DD-HH.mm.ss'
};
var logger = require('simple-node-logger')
var flog = logger.createRollingFileLogger( opts );
//flog.opts = opts;
flog.info("Started");

module.exports = flog;
