var appErrors = {};
module.exports = appErrors;

/*

https://blog.philipphauer.de/restful-api-design-best-practices/#check-out-json-api

2xx: Success
200 OK
201 Created
3xx: Redirect
301 Moved Permanently
304 Not Modified
4xx: Client Error
400 Bad Request
401 Unauthorized
403 Forbidden
404 Not Found
410 Gone
5xx: Server Error
500 Internal Server Error

*/

/*
--------------------------------------------------
    ERRORS A LA BD A PARTIR DEL CODI 100
--------------------------------------------------
*/


/*
--------------------------------------------------
    ERRORS D'AUTENTICACIÓ A PARTIR DEL CODI 200
--------------------------------------------------
*/

appErrors['200'] = {
    error: {
        status: 403,
        detail: {
            en: "The request hasn't the auth header.",
            es: "La petición no tiene la cabecera de auth."
        },
        code: 200,
    }
}


appErrors['201'] = {
    error: {
        status: 401,
        detail: {
            en: "Token Expired.",
            es: "El token ha expirado."
        },
        code: 201,
    }
}


appErrors['202'] = {
    error: {
        status: 400,
        detail: {
            en: "Invalid token",
            es: "El token es inválido."
        },
        code: 202,
    }
}

appErrors['204'] = {
    error: {
        status: 405,
        detail: {
            en: "The user authenticated isn't valid. Renew the session.",
            es: "El usuario autenticado no es válido. Renueva la sesión."
        },
        code: 204,
    }
}


/*
--------------------------------------------------
    ERRORS DE PETICIONS PARTIR DEL CODI 400
--------------------------------------------------
*/

appErrors['402'] = {
    error: {
        status: 400,
        detail: {
            en: "Please, fill all the fields.",
            es: "Rellene todos los campos, por favor."
        },
        code: 402,
    }
}

appErrors['403'] = {
    error: {
        status: 404,
        detail: {
            en: "Client not found.",
            es: "No se ha encontrado el cliente indicado."
        },
        code: 403,
    }
}

appErrors['405'] = {
    error: {
        status: 400,
        detail: {
            en: "Error on get the client list from url.",
            es: "Error en obtenir la llista dels clients de la url."
        },
        code: 405
    }
}

appErrors['406'] = {
    error: {
        status: 400,
        detail: {
            en: "The name of any field is incorrect.",
            es: "El nom d'algún camp és incorrecte."
        },
        code: 406
    }
}

appErrors['407'] = {
    error: {
        status: 400,
        detail: {
            en: "You are not allowed to see this.",
            es: "No tens permisos per fer aquesta consulta."
        },
        code: 407
    }
}
