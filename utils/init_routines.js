var consts = require('./general_consts');
var fs = require('fs');

var initRoutines = {};

/*
    CODIS ERROR DE SORTIDA DEL NODE

    10001 ->    No s'ha pogut crear la carpeta temporal TMP
    10002 ->    En crear les carpetes de resources dels clients, hi ha
                hagut un error a la bd.
    10003 ->    En crear les carpetes de resources dels clients hi ha hagut
                un error al SO.
    10004 ->    En crear la carpeta de logs del logger.

*/

initRoutines.createLogsDirectory = function() {
    var dir = consts.logsDir;
    if (!fs.existsSync(dir)) {
        //flog.info('info',"Creating the tmpdir: " + dir);
        fs.mkdirSync(dir);
        if (!fs.existsSync(dir)) {
            //flog.info('err', "Error on create the tmpdir, exiting node");
            /*
                Si cal, definir els codis d'error que siguin mitjançant sortida
                del programa.
            */
            process.exit(10004);
        }
    }
}

initRoutines.execute = function() {
    for(var fun in initRoutines) {
        if (fun != 'execute') {
            initRoutines[fun]();
        }
    }
}

module.exports = initRoutines.execute;
