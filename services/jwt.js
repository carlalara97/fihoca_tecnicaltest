'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
var secret = 'secret_key_autentification_on_fihoca_tecnical_test';

exports.createToken = function(clients){
//  console.log("JWT users : " + JSON.stringify(users));
  var payload = {
    useRole: clients.useRole,
    idClient: clients.idClient,
    iat: moment().unix(),
    exp: moment().add(30, 'days').unix()
  };
  //console.log("JWT PAYLOAD: " + JSON.stringify(payload));
  return jwt.encode(payload, secret);
};
