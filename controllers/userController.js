'use strict'

//var express = require('express');
var userModel = require('../models/user');
var appErrors = require('../utils/errors');
var flog = require('../utils/file_logger');
var path = require('path');
//var functions = require('../utils/general_functions');
var jwt = require('../services/jwt');
//var fs = require('fs');

var userController = {};


userController.loginParameters = {
  'email':null
}

userController.loginuser = function(req,res) {
  var parameters = req.body;

  var fieldNecessary = true;
  for (var prop in parameters) {
    fieldNecessary = (userController.loginParameters.hasOwnProperty(prop));
    if (!fieldNecessary) {
      break;
    }
  }

  if(!fieldNecessary) {   //campos innecesarios en la request para obtener el token
    var err = appErrors['406'];
    res.status(err.error.status).send(err);
  } else {

    if (parameters.email != "" && parameters.email != undefined) {
      userModel.getClientByEmail(parameters.email, function (error, data) {
        if (error == null) {
          if (data != null) {   // Client with specific email, exists

            // Needed information to create the token associated
            var userData = {
              useRole: data.role,
              idClient: data.id
            }
            console.log(userData);
            var tkn = jwt.createToken(userData);

            //returns no error found and an array that contains the user token and his information obtained with the query
            res.status(200).send({authToken : tkn});

          } else {
            var err = appErrors['403']; //there's not a client with that email
            res.status(err.error.status).send(err);
          }
        } else {
          if (error.code == 1) {  //error read data operation
            var err = appErrors['405'];
            res.status(err.error.status).send(err);
          }
        }
      });
    } else {
      var err = appErrors['402']; //empty data
      res.status(err.error.status).send(err);
    }
  }
}


userController.getDataID = function(req, res) {
  var id = req.user.idClient;

  userModel.getClientById(id, function(error, data) {
    if (!error) {
      if (data[0]) {
        res.status(200).send(data);
      } else {
        var err = appErrors['403']; //there's not a client with that id. Something's wrong with the token
        res.status(err.error.status).send(err);
      }
    } else {
      if (error.code == 1) {  //error read data operation
        var err = appErrors['405'];
        res.status(err.error.status).send(err);
      }
    }
  });
}


userController.getDataName = function(req, res) {
  var id = req.user.idClient;
  var name = req.body.name;

  if (name != "" && name != undefined) {
    userModel.getClientByIdName(name, id, function(error, data) {
      if (!error) {
        if (data[0]) {
          res.status(200).send(data);
        } else {
          var err = appErrors['403']; //there's not a client with that id and name.
          res.status(err.error.status).send(err);
        }
      } else {
        if (error.code == 1) {  //error read data operation
          var err = appErrors['405'];
          res.status(err.error.status).send(err);
        }
      }
    });
  } else {
    var err = appErrors['402']; //empty data
    res.status(err.error.status).send(err);
  }
}

module.exports = userController;
