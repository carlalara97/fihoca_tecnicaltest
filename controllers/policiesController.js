'use strict'

var policiesModel = require('../models/policies');
var userModel = require('../models/user');
var appErrors = require('../utils/errors');
var flog = require('../utils/file_logger');
var jwt = require('../services/jwt');

var policiesController = {};


policiesController.listAssociatedToUserName = function(req, res) {
  var id = req.user.idClient;
  var role = req.user.useRole;

  if (role == "admin") {
    policiesModel.getListByIdClient(id, function(error, data) {
      console.log(error);
      if (!error) {
        if (data[0]) {
          console.log(data);
          res.status(200).send(data);
        } else {
          var err = appErrors['403']; //there's not a police with that id related.
          res.status(err.error.status).send(err);
        }
      } else {
        if (error.code == 1) {  //error read data operation
          var err = appErrors['405'];
          res.status(err.error.status).send(err);
        }
      }
    });
  } else {
    var err = appErrors['407']; //not admin
    res.status(err.error.status).send(err);
  }
}


policiesController.getUserLinked = function(req, res) {
  var idC = req.user.idClient;
  var role = req.user.useRole;
  var idP = req.params.id;

  if (role == "admin") {
    policiesModel.getPoliceByPoliceId(idP, function(error, id) {
      if (!error) {
        if (id) {
          userModel.getClientById(id.clientId, function(err, data) {
            console.log(err);
            console.log(data);
            if (!err) {
              if (data[0]) {
                res.status(200).send(data);
              } else {
                var err = appErrors['403']; //there's not a client with that id. Something's wrong with the token
                res.status(err.error.status).send(err);
              }
            } else {
              if (err.code == 1) {  //error read data operation
                var err = appErrors['405'];
                res.status(err.error.status).send(err);
              }
            }
          });
        } else {
          var err = appErrors['403']; //there's not a police with that id.
          res.status(err.error.status).send(err);
        }
      } else {
        if (error.code == 1) {  //error read data operation
          var err = appErrors['405'];
          res.status(err.error.status).send(err);
        }
      }
    });
  } else {
    var err = appErrors['407']; //not admin
    res.status(err.error.status).send(err);
  }
}


module.exports = policiesController;
