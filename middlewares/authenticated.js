'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
var secret = 'secret_key_autentification_on_fihoca_tecnical_test';
var flog = require('../utils/file_logger');
var appErrors = require('../utils/errors');

var functs = {};


var sharedFunctions = function(header, callback) {
  if (!header){
    var err = appErrors['200'];
    callback(err, null);
  } else {
    var token = header.replace(/['"]+/g, '');

    try{
      var payload = jwt.decode(token, secret);
      if (payload.exp <= moment().unix()){
        var err = appErrors['201'];
        callback(err, null)
      } else {
        callback(null, payload);
      }
    } catch(ex){
      console.log(ex);
      var err = appErrors['202'];
      callback(err, null);
    }
  }

}


functs.ensureAuth = function(req, res, next){
  sharedFunctions(req.headers.authorization, (err, payload) => {
    if (err) {
      res.status(err.error.status).send(err);
    } else {
      if (!payload.hasOwnProperty('useRole')) {
        var error = appErrors['204'];
        res.status(error.error.status).send(error);
      } else {
        req.user = payload;
        next();
      }
    }
  });
}


/**
 * The function to check if the headers has the authorization
 *  field and if it's correct.
 * @param {*} headers the headers to check.
 * @param {*} callback the callback to call when finishes the operation,
 *  the callback will call with this arguments:
 *    (appError, paylpad)
 *  if appError is null, the headers it's correct and the second argument
 *    payload has the payload of the token.
 *  if appError is not null, appError contains the corresponding appError.
 */
functs.ensureAuthTokenByHeaders = function(headers, callback) {
  if (!headers['authorization']) {
    var apperr = appErrors['200'];
    callback(apperr, null);
  } else {
    try {
      var token = headers['authorization'];
      var payload = jwt.decode(token, secret);

      if (payload.exp <= moment().unix()){
        var apperr = appErrors['201'];
        callback(apperr, null);
      }
      callback(null, payload);
    } catch(ex) {
      var apperr = appErrors['202'];
      console.log("EL TOKEN HA FALLAT PER LO SEGUENT:");
      console.log(ex);
      console.log(apperr);
      callback(apperr, null);
    }
  }
}

functs.getPayloadFromToken = function(token) {
    return jwt.decode(token, secret);
}


module.exports = functs;
