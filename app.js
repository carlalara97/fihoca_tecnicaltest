'use strict'

var express = require('express');
//extract the entire body portion of an incoming request stream and exposes it on  req.body.
var bodyParser = require('body-parser');
var cors = require('cors');

var app = express();

//load all necessary routes
var users_routes = require('./routes/userRoutes');
var policies_routes = require('./routes/policiesRoutes');


//allows to configure api security about another domains trying to make requests against our web api.
app.use(cors());
//50MB upload maximum on every request's body
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit: 1000000}));

//configure http headers
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST');
  res.header('Allow', 'GET, POST');
  next();
});

var root = '/api/v1.0';

//base routes
app.use(root, users_routes);
app.use(root, policies_routes);

module.exports = app;
