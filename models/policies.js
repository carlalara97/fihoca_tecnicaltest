'use strict'

//var db = require('../db');
var flog = require('../utils/file_logger');
var getJSON = require('get-json');

var url = "http://www.mocky.io/v2/580891a4100000e8242b75c5";

var policiesModel = {};

policiesModel.getClientsJSON = function(callback) {
  getJSON(url)
    .then(function(response) {
      callback(null, response);
    }).catch(function(error) {
      callback(error, null);
    });
}


policiesModel.getListByIdClient = function(id, callback) {
  policiesModel.getClientsJSON(function(err, js) {
      if (!err) {
        //console.log(js);
          var filtered = [];

          let i = 0;
          for(i; i < (js.policies).length; ++i) {
             var ts = js.policies[i];
             if (ts["clientId"] == id) filtered.push(ts);
          }

          if (filtered[0]) {
            callback(null, filtered);
          }
          callback (null, null);
      } else {
        callback({code:1, message:"Can not get the client list from url."}, null);
      }
  });
}

policiesModel.getPoliceByPoliceId = function(id, callback) {
  policiesModel.getClientsJSON(function(err, js) {
      if (!err) {
          var filtered = [];

          let i = 0;
          for(i; i < (js.policies).length; ++i) {
             var ts = js.policies[i];
             if (ts["id"] == id) filtered.push(ts);
          }

          if (filtered[0]) {
            callback(null, filtered[0]);
          }
          callback (null, null);
      } else {
        callback({code:1, message:"Can not get the client list from url."}, null);
      }
  });
}


module.exports = policiesModel;
