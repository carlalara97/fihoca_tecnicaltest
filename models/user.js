'use strict'

//var db = require('../db');
var flog = require('../utils/file_logger');
var getJSON = require('get-json');
//var generalFunctions = require('./utils/general_functions')

var url = "http://www.mocky.io/v2/5808862710000087232b75ac";

var userModel = {};

userModel.getClientsJSON = function(callback) {
  getJSON(url)
    .then(function(response) {
      callback(null, response);
    }).catch(function(error) {
      callback(error, null);
    });
}

/**
  * Get a list of all the user information with an especific email and client
  *
  * @param {string} email of the user we gant to find
  * @param {integer} idClient id of the user client we are loking for
  * @param {function} callback the return function.
  * @returns a list of the information of the unique client found or null if there's no client found
  * @throws -error: null if there's not error or error otherwise
  *                        errorcodes-> 1: error on do the select operation
  *                                     1000: error on db connection
*/
userModel.getClientByEmail = function(email, callback) {
  userModel.getClientsJSON(function(err, js) {
      if (!err) {
          var filtered = [];

          let i = 0;
          for(i; i < (js.clients).length; ++i) {
             var ts = js.clients[i];
             if (ts["email"] == email) filtered.push(ts);
          }

          if (filtered[0]) {
            callback(null, filtered[0]);
          }
          callback (null, null);
      } else {
        callback({code:1, message:"Can not get the client list from url."}, null);
      }
  });
}

userModel.getClientById = function(id, callback) {
  userModel.getClientsJSON(function(err, js) {
      if (!err) {
          var filtered = [];

          let i = 0;
          for(i; i < (js.clients).length; ++i) {
             var ts = js.clients[i];
             if (ts["id"] == id) filtered.push(ts);
          }

          if (filtered[0]) {
            callback(null, filtered);
          }
          callback (null, null);
      } else {
        callback({code:1, message:"Can not get the client list from url."}, null);
      }
  });
}

userModel.getClientByIdName = function(name, id, callback) {
  userModel.getClientsJSON(function(err, js) {
      if (!err) {
          var filtered = [];

          let i = 0;
          for(i; i < (js.clients).length; ++i) {
             var ts = js.clients[i];
             if (ts["id"] == id && ts["name"] == name) filtered.push(ts);
          }

          if (filtered[0]) {
            callback(null, filtered);
          }
          callback (null, null);
      } else {
        callback({code:1, message:"Can not get the client list from url."}, null);
      }
  });
}


module.exports = userModel;
