## Start Demo

1. Node and npm must be installed.
2. Run **npm install** on your cmd.
3. Run **npm start** to start with the project
4. You can use postam to test

The aviable routes are the following:
1. http://localhost:3977/api/v1.0/users/loginuser : You have to add a JSON as a body with only the parameter "email"
2. http://localhost:3977/api/v1.0/users/getDataID : You only have to pass the token recived from 1 as a header authorization
3. http://localhost:3977/api/v1.0/users/getDataName : You have to pass the token  recived from 1 as a header authorization and a JSON as a body with only the parameter "email"
4. http://localhost:3977/api/v1.0/policies/listAssociatedToUserName : You only have to pass the token recived from 1 as a header authorization
5. http://localhost:3977/api/v1.0/policies/getUserLinked/:id : You only have to pass the token recived from 1 as a header authorization and as a request parameter you have to pass the police id


Actualy, there's some kind of problem i've been hours trying to solve but unfortunately i could not resolve it. Si it's still present and it
makes the service 5 to break down. The error is generated when the data from the url is read.
It would be solved if I added a data base, but because of the lack of time i was not able to introduce it.
The data base would be stored in AWS and to get the information of clients an polices i would have to connect to it.
I have the code done but not included here because it would be unusable.
For the same reason that i was not able to solve that problem, i was not able to do the test too. If i could, i would have done with mocha.
