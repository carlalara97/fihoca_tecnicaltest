'use strict'

var express = require('express');
var policiesController = require('../controllers/policiesController');
var md_auth = require('../middlewares/authenticated');
var flog = require('../utils/file_logger');

var api = express.Router();

/* Gets a list of the policies related to the admin calling this service */
api.get('/policies/listAssociatedToUserName', md_auth.ensureAuth, policiesController.listAssociatedToUserName);

/* Gets the user related to a certain policie id */
api.get('/policies/getUserLinked/:id', md_auth.ensureAuth, policiesController.getUserLinked);


module.exports = api;  //exports all functions included on api
