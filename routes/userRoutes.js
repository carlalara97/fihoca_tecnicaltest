'use strict'

var express = require('express');
var userController = require('../controllers/userController');
var md_auth = require('../middlewares/authenticated');
var flog = require('../utils/file_logger');

var api = express.Router();

/* Logs the user with his email to get the token */
api.post('/users/loginuser', userController.loginuser);

/* Gets the user data filtered by it's id present in the token */
api.get('/users/getDataID', md_auth.ensureAuth, userController.getDataID);

/* Gets the user data filtered by it's name, passed as a parameter */
api.get('/users/getDataName', md_auth.ensureAuth, userController.getDataName);


module.exports = api;  //exports all functions included on api
